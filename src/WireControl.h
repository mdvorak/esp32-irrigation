#pragma once

#include <Wire.h>
#include <esp32-hal-i2c.h>

class WireControlClass
{
  public:
    void begin(uint8_t sda, uint8_t scl, bool suspend = false)
    {
        // Store used pins
        _sda = sda;
        _scl = scl;

        // Init Wire library
        Wire.begin(sda, scl);
        // Note: We re-init i2c to get pointer - it always returns same pointer to internal structure
        _i2c = i2cInit(0, sda, scl, 0);

        // Detach immediately if requested
        if (suspend)
        {
            this->suspend();
        }
    }

    void suspend()
    {
        // Flush any pending data
        Wire.flush();

        // Detach I2C controller from pins
        i2cDetachSDA(_i2c, _sda);
        i2cDetachSCL(_i2c, _scl);

        // Detaching leaves pins in INPUT_PULLUP mode, so set them to INPUT
        pinMode(_sda, INPUT);
        pinMode(_scl, INPUT);
    }

    void resume()
    {
        // Init also sets correct mode for both pins
        i2cInit(0, _sda, _scl, 0);
        Wire.flush();
    }

  private:
    uint8_t _sda;
    uint8_t _scl;
    i2c_t *_i2c;
};

// Singleton
WireControlClass WireControl;
