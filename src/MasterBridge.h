#pragma once
#include <WidgetBridge.h>

class MasterBridge
{
  public:
    MasterBridge(int bridgePin)
        : _bridge(bridgePin)
    {
    }

    void config(const String &auth, uint8_t offset)
    {
        _auth = auth;
        _offset = offset;
    }

    void connect()
    {
        _bridge.setAuthToken(_auth.c_str());
    }

    void virtualWrite(int pin, const char *value)
    {
        _bridge.virtualWrite(_offset + pin, value);
    }

    void virtualWrite(int pin, int value)
    {
        _bridge.virtualWrite(_offset + pin, value);
    }

    void virtualWrite(int pin, float value)
    {
        _bridge.virtualWrite(_offset + pin, value);
    }

  private:
    WidgetBridge _bridge;
    String _auth;
    uint8_t _offset;
};
