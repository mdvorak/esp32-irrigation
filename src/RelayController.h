#pragma once

#include <Arduino.h>

class RelayController
{
  public:
    RelayController(uint8_t pin)
        : _pin(pin)
    {
    }

    

  private:
    uint8_t _pin;
};
