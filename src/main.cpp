#ifdef DEBUG_ESP_PORT
#define BLYNK_PRINT DEBUG_ESP_PORT
#endif

#include <WiFi.h>
#include <SPIFFS.h>
#include <SPI.h>
#include <Ticker.h>
#include <EEPROM.h>
#include <EspIotBlynk.h>
#include <BlynkSimpleEsp32_SSL.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADS1015.h>
#include <Adafruit_BME280.h>

#ifndef NDEBUG
#include <ArduinoOTA.h>
#endif

#include "WireControl.h"
#include "MasterBridge.h"
#include "Schedule.h"
#include "Settings.h"

// Config
const uint8_t PIN_RELAY = 13;
const uint8_t PIN_SENSORS = 14;
const uint8_t PIN_SDA = 26;
const uint8_t PIN_SCL = 27;
const int MY_BME280_ADDRESS = 0x76;
const char *VALUE_NA = "n/a";

// Globals
static WatchDog watchdog(60000);
static SimpleTimer timer;
static int timerReadSensors = -1;
static Adafruit_ADS1015 ads;
static Adafruit_BME280 bme;
static bool bmeEnabled = false;
static MasterBridge master(V91);
static Settings settings = {0};
static volatile float averageMoisture = NAN;
static Ticker waterShutoffTimer;
static volatile bool waterStateChanged = false;

// Declarations
void readSensors();

// Setup
void setup()
{
	// Init
	DEBUG_ESP_BEGIN(115200);
	randomSeed(analogRead(39)); // Unconnected pin
	SPIFFS.begin();

	// Signalize start (since we lack proper on-led)
	digitalWrite(PIN_SENSORS, HIGH);

	// Configure
	pinMode(PIN_RELAY, OUTPUT);
	pinMode(PIN_SENSORS, OUTPUT);

	WireControl.begin(PIN_SDA, PIN_SCL, false);

	ads.setGain(GAIN_ONE);
	ads.setSPS(ADS1015_DR_3300SPS);

	// Detect devices
	Wire.beginTransmission(ADS1X15_ADDRESS);
	if (Wire.endTransmission() != I2C_ERROR_OK)
	{
		DEBUG_ESP_LOG1(F("ERROR: ADS1015 not detected on I2C bus"));
	}

	if (bme.begin(MY_BME280_ADDRESS))
	{
		DEBUG_ESP_LOG1(F("Found BME280 on I2C"));
		bmeEnabled = true;
	}
	else
	{
		DEBUG_ESP_LOG1(F("BME280 not detected"))
	}

	// Configure Blynk (note: returned values are intentionally never deallocated)
	BlynkSetup(Blynk,
			   ReadConfigCharsFromFS("BLYNK_AUTH") ?: "",
			   ReadConfigCharsFromFS("BLYNK_DOMAIN") ?: BLYNK_DEFAULT_DOMAIN,
			   ReadConfigStringFromFS("BLYNK_PORT").toInt() ?: BLYNK_DEFAULT_PORT_SSL,
			   ReadConfigCharsFromFS("BLYNK_ROOT_CA") ?: BLYNK_DEFAULT_ROOT_CA);

	// Init Time
	NtpInit(ReadConfigStringFromFS("NTP_SERVERS"), ReadConfigStringFromFS("TZ_INFO"));

	// Enable HTTP updates
	if (HttpUpdate.begin(ReadConfigStringFromFS("HTTP_UPDATE_URL"), ReadConfigStringFromFS("HTTP_UPDATE_CA")))
	{
		// Schedule update and randomize the interval by 30 seconds
		HttpUpdate.schedule(ReadConfigStringFromFS("HTTP_UPDATE_INTERVAL", "3600000").toInt() + random(30000));
	}

	// Complete WiFi setup (note: 1=WPS)
	WiFiHomeSetup(ReadConfigEnumFromFS("WIFI_SETUP_MODE", WIFI_HOME_SETUP_WPS));

	// OTA in debug mode only
#ifndef NDEBUG
	ArduinoOTA.onStart([]() {
		DEBUG_ESP_LOG1("OTA started");
		watchdog.suspend();
	});
	ArduinoOTA.onEnd([]() {
		DEBUG_ESP_LOG1("OTA end");
	});
	ArduinoOTA.onError([](ota_error_t err) {
		DEBUG_ESP_LOG2("OTA error: ", err);
		watchdog.resume();
	});

	ArduinoOTA.setPassword(ReadConfigStringFromFS("OTA_PASSWORD").c_str());
	ArduinoOTA.begin();
	BLYNK_LOG1("OTA updates enabled");
#endif

	// SPIFFS no longer needed
	SPIFFS.end();

	// Settings
	EEPROM.begin(sizeof(Settings));
	EEPROM.get(0, settings);
	DEBUG_ESP_LOG2(F("Loaded "), settings.toString());

	// Defaults
	if (settings.version != Settings::VERSION)
	{
		settings = {0};
		settings.version = Settings::VERSION;

		settings.readInterval = 30000;
		settings.minInterval = 20 * 60000;
		settings.maxDuration = 10 * 60000;
		settings.minMoisture = 40;
		settings.maxMoisture = 80;
		settings.thresholdMoisture = 70;

		for (int i = 0; i < SENSOR_COUNT; i++)
		{
			settings.sensors[i].enabled = false;
			settings.sensors[i].dry = 2.8f;
			settings.sensors[i].submerged = 1.5f;
		}

		EEPROM.put(0, settings);
		EEPROM.commit();
		DEBUG_ESP_LOG2(F("Using Default "), settings.toString());
	}

	// Read sensors timer
	timerReadSensors = timer.setInterval(settings.readInterval, readSensors);

	// Uptime (ping) timer
	// TODO nechat?
	timer.setInterval(60000, []() {
		struct tm tm;
		getLocalTime(&tm, 0);

		char timestr[10] = {0};
		strftime(timestr, sizeof(timestr), "%R:%S", &tm);

		BLYNK_LOG2("Current time: ", timestr)
		Blynk.virtualWrite(V95, timestr);
		Blynk.virtualWrite(V96, millis());
	});

	// TODO relay + settings
	// TODO auto off power

	// Suspend I2C
	delay(100);
	WireControl.suspend();
	digitalWrite(PIN_SENSORS, LOW);

	// Setup complete
	watchdog.begin();
	DEBUG_ESP_LOG1(F("Setup done"));
}

void loop()
{
	// Timers
	timer.run();

	// Report relay state
	if (waterStateChanged)
	{
		waterStateChanged = false;
		Blynk.virtualWrite(V9, digitalRead(PIN_RELAY) ? 1 : 0);
	}

	// Main loop
	Blynk.run();
	watchdog.heartbeat();

	// OTA in debug mode
#ifndef NDEBUG
	ArduinoOTA.handle();
#endif
}

void turnOnWater()
{
	digitalWrite(PIN_RELAY, HIGH);
	waterStateChanged = true;

	waterShutoffTimer.once_ms(settings.maxDuration, []() {
		digitalWrite(PIN_RELAY, LOW);
		waterStateChanged = true;
	});
}

void saveSettings()
{
	// Compare
	uint8_t prev[sizeof(Settings)];
	EEPROM.readBytes(0, prev, sizeof(Settings));

	if (memcmp(prev, &settings, sizeof(Settings)) != 0)
	{
		DEBUG_ESP_LOG2("Saving ", settings.toString());
		EEPROM.put(0, settings);
		EEPROM.commit();
	}
	else
	{
		DEBUG_ESP_LOG1("Settings not changed")
	}
}

BLYNK_CONNECTED()
{
	DEBUG_ESP_LOG1(F("Blynk connected"));

	Blynk.syncAll();
	master.connect();

	// Report relay state
	Blynk.virtualWrite(V9, digitalRead(PIN_RELAY) ? 1 : 0);
	waterStateChanged = false;

	// If BME is disabled, reset values
	if (!bmeEnabled)
	{
		Blynk.virtualWrite(V5, VALUE_NA);
		Blynk.virtualWrite(V6, VALUE_NA);
		Blynk.virtualWrite(V7, VALUE_NA);

		master.virtualWrite(V5, VALUE_NA);
		master.virtualWrite(V6, VALUE_NA);
		master.virtualWrite(V7, VALUE_NA);
	}
}

BLYNK_INPUT(V10)
{
	digitalWrite(PIN_RELAY, getValue.asInt() ? HIGH : LOW);
}

BLYNK_OUTPUT(V98)
{
	Blynk.virtualWrite(V98, WiFi.localIP().toString().c_str());
}

BLYNK_OUTPUT(V99)
{
	Blynk.virtualWrite(V99, WiFi.RSSI());
}

// Sensors macro (repetitive use)
#define SENSOR_CONFIG_WRITE(index, pinEnabled, pinDry, pinSubmerged) \
	BLYNK_INPUT(pinEnabled)                                          \
	{                                                                \
		bool enabled = (getValue.asInt() != 0);                      \
		settings.sensors[index].enabled = enabled;                   \
		saveSettings();                                              \
		if (!enabled)                                                \
		{                                                            \
			Blynk.virtualWrite(index, VALUE_NA);                     \
			master.virtualWrite(index, VALUE_NA);                    \
		}                                                            \
	}                                                                \
	BLYNK_INPUT(pinDry)                                              \
	{                                                                \
		settings.sensors[index].dry = getValue.asFloat();            \
		saveSettings();                                              \
	}                                                                \
	BLYNK_INPUT(pinSubmerged)                                        \
	{                                                                \
		settings.sensors[index].submerged = getValue.asFloat();      \
		saveSettings();                                              \
	}

SENSOR_CONFIG_WRITE(0, V100, V101, V102);
SENSOR_CONFIG_WRITE(1, V105, V106, V107);
SENSOR_CONFIG_WRITE(2, V110, V111, V112);
SENSOR_CONFIG_WRITE(3, V115, V116, V117);

BLYNK_INPUT(V120)
{
	settings.readInterval = constrain(getValue.asInt(), 1, 3600) * 1000UL;
	timer.changeInterval(timerReadSensors, settings.readInterval);
	saveSettings();
}

float normalizeValue(float x, float low, float high)
{
	// When both are zero.. does not handle well corner cases
	if (low == high)
	{
		return 0;
	}

	float result = (x - low) * 100.0f / (high - low);
	return constrain(result, 0.0f, 100.0f);
}

void readSensors()
{
	// Enable
	BLYNK_LOG1("Resuming I2C");
	digitalWrite(PIN_SENSORS, HIGH);
	WireControl.resume();

	if (bmeEnabled)
	{
		// Wait a little for BME to start
		delay(300);
		// Configure BME
		bme.setSampling();
	}

	// Wait for 500 ms
	delay(500);

	// Read
	BLYNK_LOG1("Reading moisture:");
	float sum = 0;
	int count = 0;

	for (int i = 0; i < 4; i++)
	{
		const auto &sensor = settings.sensors[i];
		if (sensor.enabled)
		{
			// Read sensor
			float value = ads.readADC_SingleEnded_V(i);
			// Write raw value
			Blynk.virtualWrite(i, value);

			// Normalize
			float normalized = normalizeValue(value, sensor.dry, sensor.submerged);

			// Overall
			sum += normalized;
			count++;

			// Publish
			BLYNK_LOG6("Sensor A", i, '=', value, " normalized to ", normalized);

			master.virtualWrite(i, normalized);
			delay(1);
		}
	}

	// Average
	if (count > 0)
	{
		averageMoisture = sum / (float)count;
		BLYNK_LOG2("Normalized average: ", averageMoisture);
		Blynk.virtualWrite(V4, averageMoisture);
		master.virtualWrite(V4, averageMoisture);
	}
	else
	{
		averageMoisture = NAN;
		Blynk.virtualWrite(V4, VALUE_NA);
		master.virtualWrite(V4, VALUE_NA);
		BLYNK_LOG1("All sensors disabled");
	}

	// Read BME
	if (bmeEnabled)
	{
		BLYNK_LOG1("Reading environment:");
		auto temperature = bme.readTemperature();
		auto humidity = bme.readHumidity();
		auto pressure = bme.readPressure() / 100.0f; // Convert to hPa
		BLYNK_LOG6("Temperature=", temperature, " Humidity=", humidity, " Pressure=", pressure);

		Blynk.virtualWrite(V5, temperature);
		Blynk.virtualWrite(V6, humidity);
		Blynk.virtualWrite(V7, pressure);

		master.virtualWrite(V5, temperature);
		master.virtualWrite(V6, humidity);
		master.virtualWrite(V7, (int)roundf(pressure));
	}

	// Disable
	WireControl.suspend();
	digitalWrite(PIN_SENSORS, LOW);
	BLYNK_LOG1("I2C suspended");
}
