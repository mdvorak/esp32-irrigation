#pragma once

#include <Arduino.h>

const size_t SENSOR_COUNT = 4;
const size_t SCHEDULE_COUNT = 4;

extern "C"
{
    // Configuration for one sensor
    struct SensorConfig
    {
        // Enable/disable this sensor
        bool enabled;
        // Voltage when sensor is completely dry
        float dry;
        // Voltage when sensor is submerged in the water
        float submerged;

        String &toString(String &s) const
        {
            s.concat(F("Sensor{enabled="));
            s.concat(enabled);
            s.concat(F(",dry="));
            s.concat(dry);
            s.concat(F(",submerged="));
            s.concat(submerged);
            s.concat('}');
            return s;
        }
    };

    struct Settings
    {
        // Version
        static const uint8_t VERSION = 0x1E;
        uint8_t version;

        // Sensors configuration
        SensorConfig sensors[SENSOR_COUNT];
        // Interval between reading the sensors (including temperature)
        uint32_t readInterval;

        // Minimal interval between irrigating
        uint32_t minInterval;
        // Maximal duration of irrigation
        uint32_t maxDuration;
        // When should irrigation start outside scheduled time
        uint8_t minMoisture;
        // When should irrigation stop
        uint8_t maxMoisture;
        // Don't start irrigation on schedule when over this value
        uint8_t thresholdMoisture;
        // Scheduled times of the day when irrigation should start, unless its over thresholdMoisture
        uint32_t schedule[SCHEDULE_COUNT];

        String toString() const
        {
            // Prevent resizing
            String s;
            s.reserve(275); // TODO new fields

            s.concat(F("Settings{version="));
            s.concat(String(version, HEX));
            s.concat(F(",readInterval="));
            s.concat(readInterval);
            s.concat(F(",sensors=[\n"));
            for (auto i = 0; i < SENSOR_COUNT; i++)
            {
                s.concat(' ');
                sensors[i].toString(s);
                s.concat('\n');
            }
            s.concat(F(",minInterval="));
            s.concat(minInterval);
            s.concat(F(",maxDuration="));
            s.concat(maxDuration);
            // TODO new fields
            s.concat('}');
            return s;
        }
    };
}
